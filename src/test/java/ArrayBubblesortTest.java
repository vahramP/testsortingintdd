import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;

public class ArrayBubblesortTest {

    private ArrayBubbleSort sort;

    @Before
    public void setup() {
        sort = new ArrayBubbleSort();
    }

    @Test
    public void testNull() {
        Assertions.assertThrows(NullPointerException.class, () -> sort.bubbleSort(null));
    }

    @Test
    public void testEmptyTwo() {
        int[] array = new int[Integer.MAX_VALUE - 1];

        Assertions.assertThrows(OutOfMemoryError.class, () -> sort.bubbleSort(array));
    }

    @Test(expected = NegativeArraySizeException.class)
    public void testNegativeArraySize() {
        int[] array = new int[Integer.MAX_VALUE + 1];
    }

    @Test
    public void testNormalArray() {
        int[] array = {-2, 0, 10, 36, 8};
        int[] sortedArray = sort.bubbleSort(array);
        Arrays.sort(array);

        assertArrayEquals(array, sortedArray);
    }

    @Test
    public void testDuplicates() {
        int[] array = {-2, -2, 10, 36, 8, 8, 0};
        int[] sortedArray = sort.bubbleSort(array);
        Arrays.sort(array);

        assertArrayEquals(array, sortedArray);
    }

    @Test
    public void testLargeValues() {
        int[] array = {-2, 0, 10, 36, Integer.MAX_VALUE, Integer.MIN_VALUE};
        int[] sortedArray = sort.bubbleSort(array);
        Arrays.sort(array);

        assertArrayEquals(array, sortedArray);
    }

    @Test
    public void testEmptyArray() {
        int[] array = {};
        int[] sortedArray = sort.bubbleSort(array);
        Arrays.sort(array);

        assertArrayEquals(array, sortedArray);
    }

    @Test
    public void testSwapping() {
        int input[] = {1, 2};
        sort.swap(input, 0, 1);
        int expectedOutput[] = {2, 1};

        assertArrayEquals(expectedOutput, input);
    }

}
